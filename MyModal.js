import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
} from 'react-native';

import { NAVBAR_HEIGHT } from './TopBar';

export default class MyModal extends Component {
  render() {
    return (
      <Modal animationType={'slide'}
        visible={this.props.visible}
        style={styles.container}
        onRequestClose={this.props.onClose}>
        <View style={styles.topbar}>
          <Text style={styles.text}>{NAVBAR_HEIGHT}</Text>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topbar: {
    backgroundColor: 'red',
    height: NAVBAR_HEIGHT,
  },
  text: {
    fontSize: 20
  }
});
