import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Platform,
    Text,
} from 'react-native';

import MyModal from './MyModal';

export const NAVBAR_HEIGHT = Platform.OS === 'ios' ? 200 : 100;

export default class TopBar extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>TEST</Text>
                <MyModal visible={this.props.showModal}
                    onClose={this.props.onClose} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'green',
    },
});
