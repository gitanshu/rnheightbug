import React from 'react';
import {
    View, StyleSheet, TouchableHighlight
} from 'react-native';

import TopBar, { NAVBAR_HEIGHT } from './TopBar';

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = { showModal: false };
    }

    render() {
        return (
            <TouchableHighlight onPress={this.toggleModal}>
                <View style={styles.view}>
                    <TopBar showModal={this.state.showModal}
                        onClose={this.toggleModal} />
                </View>
            </TouchableHighlight>
        );
    }

    toggleModal = () => {
        this.setState({ showModal: !this.state.showModal });
    }
}

const styles = StyleSheet.create({
    view: {
        height: NAVBAR_HEIGHT,
        backgroundColor: 'blue',
    }
});
